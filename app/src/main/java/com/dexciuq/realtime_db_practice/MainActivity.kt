package com.dexciuq.realtime_db_practice

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.dexciuq.realtime_db_practice.databinding.ActivityMainBinding
import com.dexciuq.realtime_db_practice.db.UserDao

class MainActivity : AppCompatActivity() {

    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val userDao = UserDao()
        val user = MockData.getUser()

        binding.save.setOnClickListener {
            binding.progress.isVisible = true
            userDao.saveData(user) {
                binding.progress.isVisible = false
            }
        }

        binding.get.setOnClickListener {
            userDao.getData()
        }

        binding.delete.setOnClickListener {
            userDao.removeData()
        }

        userDao.getDataLiveData.observe(this) {
            binding.user.text = it.toString()
        }

        userDao.updateLiveData.observe(this) {
            binding.userUpdate.text = it.toString()
        }
    }
}