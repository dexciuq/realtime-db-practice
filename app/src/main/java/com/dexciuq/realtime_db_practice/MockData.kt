package com.dexciuq.realtime_db_practice

import com.dexciuq.realtime_db_practice.model.Address
import com.dexciuq.realtime_db_practice.model.Hobby
import com.dexciuq.realtime_db_practice.model.User

object MockData {
    fun getUser() = User(
        id = 1,
        name = "Dinmukhammed",
        email = "dimokzhasulanov@gmail.com",
        age = 19,
        hobbies = getHobbies(),
        address = getAddress(),
    )

    private fun getHobbies() = listOf(
        Hobby(
            name = "Cooking",
            description = "Experimenting with new recipes and cuisines."
        ),
        Hobby(
            name = "Programming",
            description = "Building software and solving problems with code."
        ),
        Hobby(
            name = "Reading",
            description = "Enjoying a good book in my free time."
        ),
    )

    private fun getAddress() = Address(
        city = "Astana",
        street = "Bukeikhanov A.",
        zipCode = "010000"
    )
}