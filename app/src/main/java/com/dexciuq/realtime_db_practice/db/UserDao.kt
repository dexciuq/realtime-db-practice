package com.dexciuq.realtime_db_practice.db

import com.dexciuq.realtime_db_practice.model.User

private const val DATABASE_NAME = "user"

class UserDao : FRDBWrapper<User>() {
    override fun getDatabaseName(): String = DATABASE_NAME
    override fun getClassType(): Class<User> = User::class.java
}