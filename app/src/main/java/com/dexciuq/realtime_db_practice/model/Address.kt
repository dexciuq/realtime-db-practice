package com.dexciuq.realtime_db_practice.model

data class Address(
    val city: String? = "",
    val street: String? = "",
    val zipCode: String? = ""
)
