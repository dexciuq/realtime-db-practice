package com.dexciuq.realtime_db_practice.model

data class Hobby(
    val name: String? = "",
    val description: String? = "",
)