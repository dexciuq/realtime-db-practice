package com.dexciuq.realtime_db_practice.model

data class User(
    val id: Int? = 0,
    val name: String? = "",
    val email: String? = "",
    val age: Int? = 0,
    val hobbies: List<Hobby>? = emptyList(),
    val address: Address? = Address()
)